module top (

input clk,
output OUT,
output TEST

);

reg [23:0] color = 0;
reg [23:0] testt [1000:0];
reg [7:0] red = 255;
reg [7:0] green = 127;
reg [7:0] blue = 0;
reg [0:0] out, test;
reg [1:0] main_flag = 1;
reg [3:0] data_flag = 0;
reg [4:0] counter = 24;
reg [21:0] counter2 = 0;
reg [1:0] colorflag = 0;
reg [12:0] data_counter = 0;


always @(posedge clk) begin

	if (counter > 0 && main_flag == 1) begin
		if (color[counter - 1:counter - 1] == 1) begin
			data_flag = 1;
			main_flag = 0;
		end else if (color[counter - 1:counter - 1] == 0) begin
			data_flag = 3;
			main_flag = 0;
		end
		counter <= counter - 1;
		//counter2 = 1;
	end
	else if(counter == 0 && main_flag == 1) begin
		counter = 24;
		data_flag = 5;
		main_flag = 0;
		//counter2 <= counter2 + 1;
	end

	//one
	if(data_flag == 1 && data_counter <= 9 && main_flag == 0) begin
		out = 1'b1;
		data_counter <= data_counter + 1;
	end else if (data_flag == 1 && data_counter > 9 && main_flag == 0) begin
		data_flag = 2;
		data_counter = 0;
	end
	else if(data_flag == 2 && data_counter <= 5 && main_flag == 0) begin
		out = 1'b0;
		data_counter <= data_counter + 1;
	end else if (data_flag == 2 && data_counter > 5 && main_flag == 0) begin
		data_flag = 0;
		data_counter = 0;
		main_flag = 1;
	end

	//zero
	else if(data_flag == 3 && data_counter <= 3 && main_flag == 0) begin
		out = 1'b1;
		data_counter <= data_counter + 1;
	end else if (data_flag == 3 && data_counter > 3 && main_flag == 0) begin
		data_flag = 4;
		data_counter = 0;
	end
	else if(data_flag == 4 && data_counter <= 9 && main_flag == 0) begin
		out = 1'b0;
		data_counter <= data_counter + 1;
	end else if (data_flag == 4 && data_counter > 9 && main_flag == 0) begin
		data_flag = 0;
		data_counter = 0;
		main_flag = 1;
	end

	//reset
	else if(data_flag == 5 && data_counter <= 650 && main_flag == 0) begin
		out = 1'b0;
		data_counter <= data_counter + 1;
	end else if (data_flag == 5 && data_counter > 650 && main_flag == 0) begin
		data_flag = 0;
		data_counter = 0;
		main_flag = 1;
	end
	counter2 <= counter2 + 1;
	testt[23][1000] <= testt[23][999] + 1;
	
end

always @(posedge counter2[15:15]) begin
	if (colorflag == 0) begin
		red <= red + 1;
	end
	else if (colorflag == 1) begin
		green <= green + 1;
	end
	else if (colorflag == 2) begin
		blue <= blue + 1;
	end
	if (red == 8'b11111111) begin
		red <= red + 1;
		colorflag = 1;	
	end
	if (green == 8'b11111111) begin
		green <= green + 1;
		colorflag = 2;	
	end
	if (blue == 8'b11111111) begin
		blue <= blue + 1;
		colorflag = 0;	
	end
	color[7:0] <= blue;
	color[15:8] <= red;
	color[23:16] <= green;
end

assign OUT = out;
assign TEST = testt[23][999];
//assign color[7:0] = blue;
//assign color[15:8] = red;
//assign color[23:16] = green;
endmodule
